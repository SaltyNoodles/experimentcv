//
// Created by KentaYamagishi on 2016/11/25.
//

#include <iostream>
#include <opencv2/opencv.hpp>

#include "IrisDetect.h"
#include "DegreofCircle.h"
#include "ImgProcessing.h"

// HaarLikeで目の検出
std::vector<cv::Rect> TrimingEye(cv::Mat &image){
    const double scale = 2.5;
    cv::Mat gray,small_image(cv::saturate_cast<int>(image.rows/scale), cv::saturate_cast<int>(image.cols/scale), CV_8UC1);

    cv::cvtColor(image, gray, CV_BGR2GRAY);
    cv::resize(gray, small_image, small_image.size(), 0, 0, cv::INTER_LINEAR);

    std::string cascadeName = "/usr/local/opt/opencv3/share/OpenCV/haarcascades/haarcascade_eye.xml";
    cv::CascadeClassifier cascade;

    cascade.load(cascadeName);

    std::vector<cv::Rect> faces;

    cascade.detectMultiScale(image, faces, 1.1, 2,CV_HAAR_SCALE_IMAGE,cv::Size(50, 50));

    return faces;
}

cv::Rect get_eye(std::vector<cv::Rect> trimeye, cv::Point2i pupil_center){
    cv::Rect eyes;
    for(int i = 0; i < trimeye.size(); i++){
        if(trimeye[i].contains(pupil_center))
            if(eyes.width + eyes.height < trimeye[i].width + trimeye[i].height)
                eyes = trimeye[i];
    }
    return eyes;
}

int RANDOM(int MAX){
    return rand() % MAX;
}

std::vector<int> multi_Random(int MAX, int create_num){
    std::vector<int> ans;
    ans.push_back(RANDOM(MAX)); // １つランダム数を入れる

    for(int i = 1;i < create_num; i ++){
        while(1) {
            // 変数
            int tmp = RANDOM(MAX); // ランダムで数字を生成
            bool end_flg = true;   // while終了用flg

            // 生成済の数字と被ってないかチェック
            for (int j = 0; j < ans.size(); j++) {
                if(tmp == ans[j]) {
                    end_flg = false;
                    break;
                }
            }

            // 被ってなければベクターに挿入&Break
            if(end_flg){
                ans.push_back(tmp);
                break;
            }
        }
    }

    return ans;
}

std::vector<cv::Point2i> starburst(cv::Mat bin_img, cv::RotatedRect pupil, cv::Mat &draw ){
    std::vector<cv::Point2i> elementsPos; // 放射状に探索した結果を格納
    int feature = 0, feature2 = 255;      // 探索する2値画像の2値
    cv::Mat1i ptImg = bin_img;            // 入力画像を明示的に1channel, int型にする
    cv::Rect img_area(0, 0, bin_img.cols, bin_img.rows); // 探索する画像のサイズ
    //int begin_rad = static_cast<int>(sqrt(Square(pupil.size.width)+Square(pupil.size.height)) / 2); // 瞳孔の矩形の外接円の半径
    int begin_rad = pupil.size.height;

    for(int theta = -20; theta <= 20; theta++){ // -20度〜20度までを放射状に探索

        // 0度側の探索
        double r = begin_rad; // 半径を1ずつ増やして探索
        bool flg = false;  // 虹彩(黒) -> 強膜(白)の判定を行うためのFlg
        while(1){
            int px = static_cast<int>(r * cos(theta * (CV_PI/180)));
            int py = static_cast<int>(r * sin(theta * (CV_PI/180)));
            cv::Point2i search_pos = cv::Point2i(px + pupil.center.x , py + pupil.center.y);

            if(!img_area.contains(search_pos)) // 探索点が画像外に出たらbreak
                break;
            else if(ptImg(search_pos.y, search_pos.x) == feature && !flg) // 探索点が黒(虹彩？)ならFlgをTrueにする
                flg = true;
            else if(ptImg(search_pos.y, search_pos.x) == feature2 && flg){ // 探索点が白(強膜？)なら探索点を格納
                elementsPos.push_back(search_pos);
                if(!draw.empty())
                    cv::circle(draw, search_pos, 2, cv::Scalar(200, 200, 0), 1, 4);
                break;
            }
            r += 1.0; // 半径++
        }

        // 180度逆側の探索
        r = begin_rad;
        flg = false;
        while(1){
            int px = - static_cast<int>( r * cos(theta * (CV_PI/180)));
            int py = - static_cast<int>( r * sin(theta * (CV_PI/180)));
            cv::Point2i search_pos = cv::Point2i(px + pupil.center.x , py + pupil.center.y);

            if(!img_area.contains(search_pos))
                break;
            else if(ptImg(search_pos.y, search_pos.x) == feature && !flg)
                flg = true;
            else if(ptImg(search_pos.y, search_pos.x) == feature2 && flg){
                elementsPos.push_back(search_pos);
                if(!draw.empty())
                    cv::circle(draw, search_pos, 2, cv::Scalar(200, 200, 0), 1, 4);
                break;
            }
            r += 1.0;
        }

    }

    return elementsPos;
}

cv::RotatedRect elipse_RANSAC(std::vector<cv::Point2i> element, cv::RotatedRect pupil, cv::Mat &draw){

    // RANSAC用
    int sample_size = 20; // サンプルを生成する為の要素数
    int repeat = 3000;   // 繰り返し回数

    // 最小誤差発見用
    int min_error = 0;
    cv::RotatedRect best_elipse;

    // 描画用
    cv::RotatedRect drawbox;
    cv::Point2i drawfocus[2];
    std::vector<cv::Point2i> drawvector;


    // 左右で均等にするため
    std::vector<int> rp, lp;
    for(int i = 0; i < element.size(); i++){
        if(element[i].x < pupil.center.x)
            lp.push_back(i);
        else
            rp.push_back(i);
    }
    // 均等ここまで

    for(int i = 0; i < repeat; i++){

        std::vector<cv::Point2i> sample_element; // サンプル作成用の要素

        //-----------------------
        // サンプル作成
        //-----------------------
        std::vector<int> sample_num;
        //std::vector<int> sample_num = multi_Random(element.size()-1, sample_size); // ランダム整数生成

        // サンプルを生成する為の要素番号をランダムで生成( 個数は左右で均等にする )
        std::vector<int> l_num = multi_Random(lp.size()-1, sample_size/2); // ランダム整数生成
        std::vector<int> r_num = multi_Random(rp.size()-1, sample_size/2); // ランダム整数生成
        for(int m = 0; m < sample_size/2; m ++){
            int L = lp[l_num[m]], R = rp[r_num[m]];
            sample_num.push_back(L);
            sample_num.push_back(R);
        }

        // ランダムで選択した要素から楕円の推定
        std::sort(sample_num.begin(), sample_num.end()); // クイックソート
        for(int j = 0; j < sample_size; j++)
            sample_element.push_back(element[sample_num[j]]);
        cv::RotatedRect sample_box = cv::fitEllipse(sample_element);

        // Starburstが横軸のみの為，横軸直径の真円として，要素との相関
        sample_box.angle = pupil.angle;
        sample_box.size.height = sample_box.size.width;

        // 楕円のパラメータ
        int longVec  = sample_box.size.height / 2; // 長軸
        int shortVec = sample_box.size.width / 2;  // 短軸
        int angle = sample_box.angle;              // 楕円の傾き
        cv::Point2i center = sample_box.center;    // 楕円中心
        cv::Point2i focus[2];                      // 楕円の焦点
        int base_focus = sqrt(Square(longVec) - Square(shortVec)); // 傾き0のときの楕円焦点

        focus[0] = cv::Point2i(center.x - base_focus * sin(angle * CV_PI/180), center.y + base_focus * cos(angle * CV_PI/180));
        focus[1] = cv::Point2i(center.x + base_focus * sin(angle * CV_PI/180), center.y - base_focus * cos(angle * CV_PI/180));

        //-----------------------
        // 誤差比較
        //-----------------------
        //int num = 0;
        double error = 0.0;

        for(int k = 0; k < element.size(); k++){
            // 誤差の計算
            double tmp1 = sqrt(Square(element[k].x - focus[0].x) + Square(element[k].y - focus[0].y));
            double tmp2 = sqrt(Square(element[k].x - focus[1].x) + Square(element[k].y - focus[1].y));

            error += std::abs((2 * longVec)- (tmp1 + tmp2));
        }

        // すでにある値と誤差比較
        if((error < min_error || i == 0)){
            min_error = error;
            sample_box.size.width = sample_box.size.height * (pupil.size.width / pupil.size.height); // test
            best_elipse = sample_box;

            //描画用
            drawbox = sample_box;
            drawvector = sample_element;
            drawfocus[0] = focus[0];
            drawfocus[1] = focus[1];
        }
    }


    if(!draw.empty()) {
        // 線をひいてみる
        for(int j = 0; j < sample_size; j++)
            cv::line(draw, drawbox.center, drawvector[j], cv::Scalar(0,255,0), 1, 8);

        // 楕円描画
        cv::circle(draw, best_elipse.center, best_elipse.size.height / 2, cv::Scalar(255, 0, 255), 1, 8);

        float rw = best_elipse.size.width / 2, rh = best_elipse.size.height / 2, rt = best_elipse.angle;
        float cx = best_elipse.center.x, cy = best_elipse.center.y;
        cv::Point2f p10 = cv::Point(cx + rw * cos(rt * CV_PI / 180), cy + rw * sin(rt * CV_PI / 180));
        cv::Point2f p11 = cv::Point(cx + rw * cos((rt + 180) * CV_PI / 180), cy + rw * sin((rt + 180) * CV_PI / 180));
        cv::Point2f p20 = cv::Point(cx + rh * cos((rt + 90) * CV_PI / 180), cy + rh * sin((rt + 90) * CV_PI / 180));
        cv::Point2f p21 = cv::Point(cx + rh * cos((rt + 270) * CV_PI / 180), cy + rh * sin((rt + 270) * CV_PI / 180));

        cv::line(draw, p10, p11, cv::Scalar(0, 0, 255), 1, 8);
        cv::line(draw, p20, p21, cv::Scalar(255, 255, 0), 1, 8);
        cv::circle(draw, drawfocus[0], 4, cv::Scalar(255, 0, 255), 2, 8);
        cv::circle(draw, drawfocus[1], 4, cv::Scalar(255, 0, 255), 2, 8);
    }

    return best_elipse;
}

cv::RotatedRect search_elipse(cv::Mat &cl_im, cv::RotatedRect pupil, cv::Mat &draw){

    // Harrlike特徴量から目領域を算出
    std::vector<cv::Rect> eyetrim = TrimingEye(cl_im);
    cv::Rect eye = get_eye(eyetrim, pupil.center);

    // 算出した目領域で2値化
    cv::Mat roi, tmp, gray;
    cv::cvtColor(cl_im(eye), roi, CV_RGB2GRAY);
    double eye_thread = cv::threshold(roi, tmp, 0, 255, cv::THRESH_BINARY | cv::THRESH_OTSU);
    // std::cout << eye_thread << std::endl; // 大津法で導いた閾値

    // starburst( 瞳孔の傾きベースで傾き+-10~15度を探索 -> 横軸に10~15度探索に変更 )
    cv::cvtColor(cl_im, gray, CV_RGB2GRAY);
    cv::threshold(gray, tmp, eye_thread, 255, cv::THRESH_BINARY);
    std::vector<cv::Point2i> element = starburst(tmp, pupil, draw);

    // 適切な長さをRANSAC辺りで調べる( 瞳孔の中心，長軸短軸比を利用して虹彩を決定する？)
    cv::RotatedRect iris = elipse_RANSAC(element, pupil, draw);

    // 推定した虹彩の描画
    //cv::ellipse(cl_im, iris, cv::Scalar(0, 0, 255), 2, 8);

    return iris;
}
