#include <iostream>
#include <opencv2/opencv.hpp>

#include "ImgProcessing.h"
#include "IrisDetect.h"
#include "DegreofCircle.h"

cv::Mat nullmat;

std::vector<cv::Rect> TrimingEye(cv::Mat &image); // HaarLikeで目の検出
void sauvolaSimple(const cv::Mat &src, cv::Mat &dst, int kernelSize, double k, double r); // 周囲との差を見て2値化
void paste(cv::Mat &dst, cv::Mat src, cv::Rect box); // 画像を画像に貼り付ける関数

void fitcircle(std::vector<cv::Point2i> element, cv::Mat &draw){
    double xi ,yi ,x2, y2, xy, n, x3xy2, x2yy3, x2y2;
    xi = yi = x2 = y2 = xy = n = x3xy2 = x2yy3 = x2y2 = 0;

    for(int i = 0; i < element.size(); i++){
        int ex = element[i].x, ey = element[i].y;
        xi += ex;
        yi += ey;
        x2 += Square(ex);
        y2 += Square(ey);
        xy += ex * ey;
        n++;

        x3xy2 += ex*ex*ex + ex*ey*ey;
        x2yy3 += ex*ex*ey + ey*ey*ey;
        x2y2  += Square(ex) + Square(ey);
    }

    cv::Matx33d m1(x2, xy, xi, xy, y2, yi, xi, yi, n);
    cv::Matx31d m2(-x3xy2, -x2yy3, -x2y2);

    cv::Matx31d result = m1.inv() * m2;
    int a = -result(0, 0)/2;
    int b = -result(0, 1)/2;
    int rad = sqrt(Square(a) + Square(b) - result(0, 2));

    cv::circle(draw, cv::Point(a, b), rad, cv::Scalar(255,0,0), 1, 8);
}



int RANSAC_INT(std::vector<int> element){
    int total_error;
    int best_val;

    for(int i = 0; i < element.size(); i++){
        int error = 0;
        for(int j = 0; j < element.size(); j++){
            if(i != j){
                //error = element[i] - element[j];
                error += abs(element[i] - element[j]);
            }
        }
        if(error < total_error || i == 0){
            total_error = error;
            best_val = element[i];
        }
    }

    return best_val;
}

cv::Mat retinex(cv::Mat src){
    cv::Mat tmp, tmp2, use;
    src.convertTo(use, CV_32F);
    
    cv::Mat g1 = (use + 1.0) / 256.0;
    cv::GaussianBlur(g1, tmp, cv::Size(201, 201), 100);
    tmp.convertTo(tmp2, CV_32F);
    cv::Mat g2 = (tmp2 + 1.0) / 257.0;

    cv::Mat g3 = cv::Mat::zeros(g2.rows, g2.cols, CV_32F);
    for(int i = 0; i < g2.rows; i++){
        for(int j = 0;j < g2.cols; j++){
            g3.at<float>(i, j) = cv::log(g1.at<float>(i, j) / g2.at<float>(i, j));
        }
    }
    double g3_min, g3_max;
    cv::minMaxLoc(g3, &g3_min, &g3_max);

    cv::Mat g4 = ((g3 - g3_min) / (g3_max - g3_min)) * 255.0;
    cv::Mat g5;
    g4.convertTo(g5, CV_8U);

    return cv::Mat(g5);
}



cv::Mat org;
cv::Mat eye;
std::vector<cv::Rect> eye_roi;
bool roi_flg = false;

int main(int argc, const char * argv[]) {

    int key;

    //ウィンドウ生成
    cv::namedWindow("out_img", CV_WINDOW_AUTOSIZE);
    cv::namedWindow("out_img2", CV_WINDOW_AUTOSIZE);

    //画像入力
    cv::Mat input1 = cv::imread("/Users/kenta-y/Pictures/k1.png");
    //cv::Mat input1 = cv::imread("/Users/kenta-y/Desktop/retinex2.jpg");
    cv::Mat input2 = cv::imread("/Users/kenta-y/Pictures/k2.png");

    //コンストラクト
    myCV *mycv = new myCV(input1);
    myCV *mycv2 = new myCV(input2);
    myCV *mycv3;

    while(key != 'q') {      //キー入力があるまでループ
        //myCVの出力
        cv::Mat dst = mycv->getImg();
        cv::Mat dst2 = mycv2->getImg();
        cv::Mat a;

        cv::cvtColor(dst, a, CV_RGB2GRAY);
        cv::imshow("out_img", a); //描画
        cv::waitKey(-1);

        cv::Mat test = retinex(a);

        cv::imshow("out_img", test); //描画
        cv::waitKey(-1);


        //dst = input1;


        // test

        //sauvolaSimple(dst, a, 4, 4, 4);

        // test

        //dst.copyTo(dstcp);

//        if(mycv->check_mor()){
//            cv::Mat tes;
//            cv::morphologyEx(dst, tes, cv::MORPH_OPEN, cv::Mat(), cv::Point(-1,-1), 5);
//            tes.copyTo(dst2);
//        }
        static cv::Point2i pupil_center;
        static cv::RotatedRect  pupil;

        if(key == 'n'){
            pupil_center = Ask_PupilCenter(dst2, dst);
            pupil = Ask_DegreofCircle(dst2, dst);
//            cv::rectangle(dst, pupil.size, cv::Scalar(0,0,255), 2, 8);

            // 楕円描画
//            cv::ellipse(dst, pupil.elipse, cv::Scalar(0,0,255), 1, 8);
//            float rw = pupil.elipse.size.width/2, rh = pupil.elipse.size.height/2, rt = pupil.elipse.angle;
//            float cx = pupil.elipse.center.x, cy = pupil.elipse.center.y;
//            cv::Point2f p10 = cv::Point(cx + rw * cos(rt * CV_PI/180)      , cy + rw * sin(rt * CV_PI/180));
//            cv::Point2f p11 = cv::Point(cx + rw * cos((rt+180) * CV_PI/180), cy + rw * sin((rt+180) * CV_PI/180));
//            cv::Point2f p20 = cv::Point(cx + rh * cos((rt+90) * CV_PI/180) , cy + rh * sin((rt+90) * CV_PI/180));
//            cv::Point2f p21 = cv::Point(cx + rh * cos((rt+270) * CV_PI/180), cy + rh * sin((rt+270) * CV_PI/180));
//
//            cv::line(dst, p10, p11, cv::Scalar(0,255,0), 1, 8);
//            cv::line(dst, p20, p21, cv::Scalar(255,255,0), 1, 8);

            //std::cout << pupil_center << std::endl;
            //std::cout << pupil.elipse.angle << std::endl;

            search_elipse(dst, pupil, dst);

            //std::cout << pupil.elipse.size.height << ", " << pupil.elipse.size.width << pupil.elipse.angle << std::endl;
        }

        if(key == 'm'){
            pupil_center = Ask_PupilCenter(dst2, dst);
            pupil = Ask_DegreofCircle(dst2, dst);

            std::vector<cv::Rect> eyetrim = TrimingEye(dst);
            cv::Rect eye = get_eye(eyetrim, pupil.center);
            //cv::rectangle(dst, eye, cv::Scalar(200,0,0),5,8);

        }
/*
        if(!roi_flg && pupil_center != cv::Point2i(0,0)) {
            eye_roi = TrimingEye(dst);
            eye = dst(get_eye(eye_roi, pupil_center));

            mycv3 = new myCV(eye);
            roi_flg = true;
        }

        if(roi_flg){
            eye = mycv3->getImg();
            paste(dst, eye, eye_roi[0]);
        }
*/
        //描画
        cv::imshow("out_img", dst);
        cv::imshow("out_img2", dst2);

        //key = cv::waitKey(30);
        key = cv::waitKey(-1);
    }

    //デストラクト
    delete mycv;
    delete mycv2;

    return 0;
}

// 周囲との差を見て2値化
void sauvolaSimple(const cv::Mat &src, cv::Mat &dst,
                   int kernelSize, double k, double r){

    dst.create(src.size(), src.type());

    cv::Mat srcWithBorder;
    int borderSize = kernelSize / 2 + 1;
    // 画像端の処理を簡単にするため外枠を付ける
    cv::copyMakeBorder(src, srcWithBorder,
                       borderSize, borderSize, borderSize, borderSize, cv::BORDER_REPLICATE);

    for (int y = 0; y < src.rows; y++){
        for (int x = 0; x < src.cols; x++){
            // (x,y)周囲の部分行列を取得
            cv::Mat kernel = srcWithBorder(cv::Rect(x, y, kernelSize, kernelSize));
            // 部分行列の平均・標準偏差を求め、式から閾値を求める
            cv::Scalar mean, stddev;
            cv::meanStdDev(kernel, mean, stddev);
            double threshold = mean[0] * (1 + k * (stddev[0] / r - 1));
            // dstに白黒を設定
            if (src.at<uchar>(y, x) < threshold)
                dst.at<uchar>(y, x) = 0;
            else
                dst.at<uchar>(y, x) = 255;
        }
    }
}

// 画像を画像に貼り付ける関数
void paste(cv::Mat &dst, cv::Mat src, cv::Rect box) {
    cv::Mat org, tmp;
    dst.copyTo(org);

    if(dst.channels() == 1 && src.channels() == 3){
        cv::cvtColor(org, tmp, CV_GRAY2BGR);
        tmp.copyTo(org);
    }

    if(dst.channels() == 3 && src.channels() == 1){
        cv::cvtColor(src, tmp, CV_GRAY2BGR);
        tmp.copyTo(src);
    }

    src.copyTo(org(box));
    org.copyTo(dst);
}

