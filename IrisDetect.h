//
// Created by KentaYamagishi on 2016/11/25.
//

#ifndef EXPERIMENTCV_IRISDETECT_H
#define EXPERIMENTCV_IRISDETECT_H

#include <opencv2/opencv.hpp>

#include "DegreofCircle.h"

cv::Rect get_eye(std::vector<cv::Rect> trimeye, cv::Point2i pupil_center);
cv::RotatedRect search_elipse(cv::Mat &cl_im, cv::RotatedRect pupil, cv::Mat &draw);

#endif //EXPERIMENTCV_IRISDETECT_H
